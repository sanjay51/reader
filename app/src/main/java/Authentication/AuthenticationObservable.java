package Authentication;

/**
 * Created by sanjav on 5/21/16.
 */
public interface AuthenticationObservable {
    public void registerObserver(AuthenticationObserver observer);
    public void removeObserver(AuthenticationObserver observer);
    public void notifyObservers(AuthenticationObserver.AUTH_CODE authCode, Object update);
}
