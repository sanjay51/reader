package Authentication;

/**
 * Created by sanjav on 5/21/16.
 */
public interface AuthenticationObserver {
    public enum AUTH_CODE { SIGNED_IN, LOGGED_OUT, AUTH_FAILED};
    public void notify(AUTH_CODE authCode, Object update);
}
