package Authentication;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.codingg.reader.HomeActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjav on 5/20/16.
 */
public class SignInAccessor implements AuthenticationObservable {
    private GoogleSignInOptions gso;
    private GoogleApiClient mGoogleApiClient;
    public static final int RC_SIGN_IN = 9001;
    private static final String TAG = "SignInActivity";

    public enum USER_DATA_CODE { DISPLAY_NAME, EMAIL, PHOTO_URL };

    private static SignInAccessor INSTANCE;
    List<AuthenticationObserver> observers = new ArrayList<>();

    private SignInAccessor() {};

    public static SignInAccessor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SignInAccessor();
        }

        return INSTANCE;
    }

    public GoogleSignInOptions getGoogleSignInOptions() {
        if (gso == null) {
            // Configure sign-in to request the user's ID, email address, and basic
            // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
            gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .build();
        }

        return gso;
    }

    public GoogleApiClient getGoogleAPIClient(Activity activity) {
        if (mGoogleApiClient == null) {
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(activity)
                    .enableAutoManage((FragmentActivity) activity /* FragmentActivity */, (GoogleApiClient.OnConnectionFailedListener) activity /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, getGoogleSignInOptions())
                    .build();
        }

        return mGoogleApiClient;
    }

    public void signIn(Activity activity) {
        Log.i(TAG, "Sign in activity started");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(getGoogleAPIClient(activity));
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOut(Activity activity) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                        }
                    }
            );
        }

        UserProfile.getInstance().clear();
        notifyObservers(AuthenticationObserver.AUTH_CODE.LOGGED_OUT, null);
    }

    public void handleSignInResult(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        if (result.isSuccess()) {
            UserProfile userProfile = UserProfile.getInstance();
            userProfile.loadFromGoogleAccount(result.getSignInAccount());

            notifyObservers(AuthenticationObserver.AUTH_CODE.SIGNED_IN, userProfile);
        } else {
            notifyObservers(AuthenticationObserver.AUTH_CODE.AUTH_FAILED, null);
            Log.i(TAG, "sign in failed ");
        }
    }

    public static boolean isSignedIn() {
        return UserProfile.getInstance().isSignedIn();
    }

    public static String getUserData(USER_DATA_CODE code) {
        if (isSignedIn()) {
            UserProfile userProfile = UserProfile.getInstance();

            switch (code) {
                case EMAIL:
                    return userProfile.getEmail();

                case DISPLAY_NAME:
                    return userProfile.getDisplayName();

                case PHOTO_URL:
                    return userProfile.getPhotoURL();

                default:
                    return null;
            }

        } else {
            return null;
        }
    }

    @Override
    public void registerObserver(AuthenticationObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(AuthenticationObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(AuthenticationObserver.AUTH_CODE authCode, Object update) {
        for (AuthenticationObserver observer : observers) {
            observer.notify(authCode, update);
        }
    }
}