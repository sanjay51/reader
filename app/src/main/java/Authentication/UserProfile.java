package Authentication;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import java.util.HashMap;
import java.util.Map;

import Firebase.FirebaseEntity;
import Firebase.ProfileEntity;

import static Utils.Constants.*;

//Keeping singleton as we will be dealing with single user at any time.

public class UserProfile {
    private String email;
    private String displayName;
    private String photoURL;

    private Context context;
    private String PREF_USER_PROFILE = "ISS_USER_PROFILE";

    private static UserProfile userProfile;
    private SharedPreferences pref;

    public static UserProfile getInstance() {
        if (userProfile == null) {
            userProfile = new UserProfile();
        }

        return userProfile;
    }

    public void initialize(Context context) {
        this.context = context;
        this.pref = this.context.getSharedPreferences(PREF_USER_PROFILE, context.MODE_PRIVATE);
        loadProfileFromLocal();
    }

    public void update(String email, String displayName, String photoURL) {
        this.email = email;
        this.displayName = displayName;
        this.photoURL = photoURL;

        save();
    }

    private void save() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_SIGNED_ID , true);
        editor.putString(EMAIL, email);
        editor.putString(DISPLAY_NAME, displayName);
        editor.putString(PHOTO_URL, photoURL);

        //save in local DB
        editor.commit();

        //save in remote DB as well
        new ProfileEntity(this).save();
    }

    public void clear() {
        pref.edit().clear().commit();
    }

    public boolean isSignedIn() {
        return pref.getBoolean(IS_SIGNED_ID, false);
    }

    private void loadProfileFromLocal() {
        boolean isSignedIn = pref.getBoolean(IS_SIGNED_ID, false);

        if (isSignedIn) {
            this.email = pref.getString(EMAIL, null);
            this.displayName = pref.getString(DISPLAY_NAME, null);
            this.photoURL = pref.getString(PHOTO_URL, null);
        }
    }

    public void loadFromGoogleAccount(GoogleSignInAccount googleAccount) {
        this.update(googleAccount.getEmail(),
                googleAccount.getDisplayName(),
                googleAccount.getPhotoUrl().toString());
    }

    public Map<String, Object> getAsMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(EMAIL, email);
        map.put(DISPLAY_NAME, displayName);
        map.put(PHOTO_URL, photoURL);

        return map;
    }

    public String getEmail() {
        return email;
    }
    public String getDisplayName() {
        return displayName;
    }
    public String getPhotoURL() {
        return photoURL;
    }
}
