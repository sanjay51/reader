package Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sanjav on 9/5/15.
 */
public class StaticData {
    public static final int SORT_NATURAL_ORDER = 1;
    public static final int SORT_RECENTLY_READ_FIRST = 2;
    public static final int SORT_UNREAD_FIRST = 3;
    public static final int SORT_BY_YOUR_RATING = 4;

    public static final Map<Integer, String> VolumeIdToFilenameMap = new HashMap<>();

    static {
        VolumeIdToFilenameMap.put(0, "temp.xml");
        VolumeIdToFilenameMap.put(1, "temp2_64.xml");
        VolumeIdToFilenameMap.put(2, "temp3_12.xml");
        VolumeIdToFilenameMap.put(3, "temp4_28.xml");
        VolumeIdToFilenameMap.put(4, "panchatantra.txt");
        VolumeIdToFilenameMap.put(5, "talesOfHitopadesha.txt");
        VolumeIdToFilenameMap.put(6, "shortStoriesAnimals.txt");
        VolumeIdToFilenameMap.put(7, "theBlueFairyBook.txt");
        VolumeIdToFilenameMap.put(8, "theRedFairyBook.txt");
        VolumeIdToFilenameMap.put(9, "theYellowFairyBook.txt");
        VolumeIdToFilenameMap.put(10, "theVioletFairyBook.txt");
        VolumeIdToFilenameMap.put(11, "temp5_161.xml");
    };

    public int getVolumeCount() {
        return  VolumeIdToFilenameMap.size();
    }
}