package Data;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import com.codingg.reader.R;

import model.MenuItem;
import model.Story;
import model.Volume;

public class StoryDataManager {
    ArrayList<Story> StoryList;
    static StoryDataManager manager[] = new StoryDataManager[11];
    int Volume;
    Context context;

    //private constructor
    private StoryDataManager(Context context, int volume) {
        StoryList = new ArrayList<Story>();
        this.context = context;
        this.Volume = volume;

        parseXML(volume, StaticData.VolumeIdToFilenameMap.get(volume));
    }

    public String getTitle(int index) {
        return StoryList.get(index).getTitle();
    }

    public Story getStory(int index) {
        return StoryList.get(index);
    }

    public boolean isLastStory(int index) {
        return index >= StoryList.size();
    }

    public ArrayList<String> getTitleList() {
        ArrayList<String> titleList = new ArrayList<String>();
        for (Story story : StoryList) {
            titleList.add(story.getTitle());
        }

        return titleList;
    }

    public ArrayList<Story> getStoryList() {
        return StoryList;
    }

    //static method
    public static ArrayList<MenuItem> getVolumeList() {
        ArrayList<MenuItem> volumeList = new ArrayList<MenuItem>();
        volumeList.add(new Volume(0, 56, "Volume.I", "Collection of 56 short stories"));
        volumeList.add(new Volume(1, 64, "Volume.II", "Collection of 64 short stories"));
        volumeList.add(new Volume(2, 32, "Volume.III", "32 short stories collection"));
        volumeList.add(new Volume(3, 28, "Volume.IV", "28 short stories"));
        volumeList.add(new Volume(11, 161, "Volume.V (new)", "New 161 short stories"));

        //Panchatantra
        Volume volPanchatantra = new Volume(4, 100, "Panchatantra", "Ancient fables collection");
        volPanchatantra.setImageResourceID(R.drawable.panchatantra);
        volPanchatantra.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volPanchatantra);

        //Tales of Hitopadesha
        Volume volHitopadesha = new Volume(5, 15, "Tales of Hitopadesha", "Classic fables originally written in Sanskrit");
        volHitopadesha.setImageResourceID(R.drawable.hitopadesha);
        volHitopadesha.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volHitopadesha);

        //Animals - short stories
        Volume volAnimalsSS = new Volume(6, 82, "Animals short stories", "Classic short stories around animals");
        volAnimalsSS.setImageResourceID(R.drawable.animal);
        volAnimalsSS.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volAnimalsSS);

        //The blue fairy book
        Volume volBlueFairyBook = new Volume(7, 35, "The Blue Fairy Book", "Fairy tales collection - 1");
        volBlueFairyBook.setImageResourceID(R.drawable.blue_fairy_book);
        volBlueFairyBook.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volBlueFairyBook);

        //The red fairy book
        Volume volRedFairyBook = new Volume(8, 12, "The Red Fairy Book", "Fairy tales collection - 2");
        volRedFairyBook.setImageResourceID(R.drawable.red_fairy_book);
        volRedFairyBook.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volRedFairyBook);

        //The yellow fairy book
        Volume volYellowFairyBook = new Volume(9, 45, "The Yellow Fairy Book", "Fairy tales collection - 3");
        volYellowFairyBook.setImageResourceID(R.drawable.yellow_fairy_book);
        volYellowFairyBook.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volYellowFairyBook);

        //The violet fairy book
        Volume volVioletFairyBook = new Volume(10, 21, "The Violet Fairy Book", "Fairy tales collection - 4");
        volVioletFairyBook.setImageResourceID(R.drawable.violet_fairy_book);
        volVioletFairyBook.setVolumeType(model.Volume.TYPE_BOOK);
        volumeList.add(volVioletFairyBook);

        manager = new StoryDataManager[volumeList.size()];
        return volumeList;
    }

    public static ArrayList<MenuItem> getOnlineContentItems() {
        ArrayList<MenuItem> itemList = new ArrayList<>();

        MenuItem item1 = new MenuItem(0, "Inspirational stories", "Requires internet");
        item1.setItemType(MenuItem.ITEM_ONLINE_CONTENT);
        item1.setImageResourceID(R.drawable.testimage1);
        itemList.add(item1);

        return itemList;
    }

    public static ArrayList<MenuItem> getOtherMenuItems() {
        ArrayList<MenuItem> itemList = new ArrayList<>();

        MenuItem itemYourProfile = new MenuItem(0, "My Profile", "My profile and sign-in");
        itemYourProfile.setItemType(MenuItem.ITEM_YOUR_PROFILE);
        itemYourProfile.setImageResourceID(R.drawable.your_profile);
        itemList.add(itemYourProfile);

        MenuItem itemBrainSuite = new MenuItem(1, "Brain Teasers + Puzzles", "Perfect way to sharpen your brain");
        itemBrainSuite.setItemType(MenuItem.ITEM_BRAIN_SUITE);
        itemBrainSuite.setImageResourceID(R.drawable.brainsuite);
        itemList.add(itemBrainSuite);

        MenuItem itemSubmit = new MenuItem (2, "Submit a story", "And we'll publish it in next release");
        itemSubmit.setItemType(MenuItem.ITEM_SUBMIT_STORY);
        itemSubmit.setImageResourceID(R.drawable.ic_submit_story_3);
        itemList.add(itemSubmit);

        MenuItem itemLike = new MenuItem(3, "Rate this app", "Please rate this app on play store");
        itemLike.setItemType(MenuItem.ITEM_LIKE);
        itemLike.setImageResourceID(R.drawable.ic_like);
        itemList.add(itemLike);

        MenuItem itemShare = new MenuItem(4, "Share this app", "Share this app with friends and family");
        itemShare.setItemType(MenuItem.ITEM_SHARE);
        itemShare.setImageResourceID(R.drawable.ic_share);
        itemList.add(itemShare);

        MenuItem itemContact = new MenuItem(5, "Contact developer", "Suggestion, feature request or even complaint, please drop an Email ");
        itemContact.setItemType(MenuItem.ITEM_CONTACT);
        itemContact.setImageResourceID(R.drawable.ic_contact);
        itemList.add(itemContact);

        return itemList;
    }

    //static method for returning manager
    public static StoryDataManager getManager(Context context, int volume) {

        if (manager[volume] == null) {
                manager[volume] = new StoryDataManager(context, volume);
                manager[volume].Volume = volume;
        }

        manager[volume].populateReadStatus(context);
        return manager[volume];
    }

    public void parseXML(int volume, String fileName) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            int storyIndex = 0;

            InputStream in_s = context.getAssets().open(fileName);

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);

            int eventType = parser.getEventType();
            Story story = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("id")) {
                            story = new Story();
                            story.index = (storyIndex++);
                            story.id = parser.nextText();
                            story.volume = "volume" + volume;
                        } else if (name.equals("title")) {
                            story.title = parser.nextText();
                        } else if (name.equals("content")) {
                            story.content = parser.nextText();
                            StoryList.add(story);
                        }

                        break;
                }

                eventType = parser.next();
            }

        } catch (Exception e) {
            Log.e("error", "error" + e.getLocalizedMessage());
        }
    }

    public void populateReadStatus(Context context) {
        UserDataManager pref = UserDataManager.getInstance(context);
        List<Integer> readList = pref.getReadStoryList(Volume);

        for (int i = 0; i< StoryList.size(); i++) {
            if (readList.contains(i)) {
                StoryList.get(i).setIsRead(true);
            } else {
                StoryList.get(i).setIsRead(false);
            }
        }
    }
}
