package Data;

import android.content.SharedPreferences;
import android.content.Context;
import android.util.Log;

import java.util.List;

/**
 * Created by sanjav on 9/5/15.
 */
public class UserDataManager {
    private final String READ_LIST = ".ReadList";
    private final String FAVORITE_LIST = ".FavoriteList";
    private final String STORY_RATING_LOCAL = ".StoryRatingLocal";

    private SharedPreferences pref = null;
    private static UserDataManager INSTANCE = null;

    private UserDataManager(Context context) {
        pref = context.getSharedPreferences("ReaderISS", context.MODE_PRIVATE);
    }

    public static void init(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserDataManager(context);
        }
    }

    public static UserDataManager getInstance(Context context) {
        if (INSTANCE == null) {
            init(context);
        }

        return INSTANCE;
    }

    public void markStoryRead(String volume, int index) {
        String prefKey = volume + READ_LIST;
        updatePreference(prefKey, index);
    }

    public List<Integer> getReadStoryList(int volume) {
        String strReadList = getCurrentPreference("volume" + volume + READ_LIST);
        return Utils.splitStringToIntegerList(strReadList);
    }

    public String getCurrentPreference(String key) {
        return pref.getString(key, null);
    }

    public void updatePreference(String prefKey, int value) {
        SharedPreferences.Editor editor = pref.edit();

        String currentValue = getCurrentPreference(prefKey);
        String newValue;

        if (currentValue == null || currentValue.isEmpty()) {
            newValue = value + "";
        } else {
            newValue = currentValue + "," + value;
        }

        editor.putString(prefKey, newValue);
        editor.commit();
    }

    public void setPreference(String prefKey, int value) {
        SharedPreferences.Editor editor = pref.edit();

        String newValue = "" + value;

        editor.putString(prefKey, newValue);
        editor.commit();
    }

    public void markStoryFavorite(String volume, int index) {
        String prefKey = volume + FAVORITE_LIST;
        updatePreference(prefKey, index);
    }

    public void setStoryRating(String volume, String storyID, int rating) {
        String prefKey = getStoryRatingKey(volume, storyID);
        setPreference(prefKey, rating);
    }

    public int getStoryRatingLocal(String volume, String storyID) {
        int rating = 0;

        try {
            String val = getCurrentPreference(getStoryRatingKey(volume, storyID));
            if (val != null) {
                rating = Integer.valueOf(val);
            }
        } catch (Exception e) {
            Log.i("rating-debug", "storyID:" + storyID);
            Log.i("rating-debug", "volume: " + volume);
            Log.i("rating-debug", "key: " + getStoryRatingKey(volume, storyID));
            Log.i("rating-debug", e.getLocalizedMessage());
            //do nothing
        }

        return rating;
    }

    private String getStoryRatingKey(String volume, String storyID) {
        return volume + "." + storyID + STORY_RATING_LOCAL;
    }
}
