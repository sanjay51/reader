package Data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.codingg.reader.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjav on 9/5/15.
 */
public class Utils {
    private static String TAG_TOAST = "Toast";

    public static String getNoAdMessage() {
        String noAdMessage = "If you like this app, please consider " +
                "buying the ad-free version (only $1), and support further development. " +
                "All the money collected, will be spent in developing new " +
                "features, and will help our developers. " +
                "Thank you.";
        return noAdMessage;
    }

    public static boolean isAdFreeVersion() {
        return true;
    }

    public static List<Integer> splitStringToIntegerList(String str) {
        //Delimiter should be comma.
        List<Integer> list = new ArrayList<Integer>();
        if (str == null || str.isEmpty()) return list;
        String[] values = str.split(",");
        for (int i = 0; i < values.length; i++) {
            String value = values[i];
            Integer val = Integer.parseInt(value);
            list.add(val);
        }
        return list;
    }

    public static void emailDeveloper(Activity activity, String subject, String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"sanjay.verma.nitk@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "(Inspirational Short Stories) " + subject);
        i.putExtra(Intent.EXTRA_TEXT, text);

        try {
            activity.startActivity(Intent.createChooser(i, "Send mail using.."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "No email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    static List<Integer> colorList = new ArrayList<>();

    public static List<Integer> getDarkColorList() {

        if (colorList.isEmpty()) {
            colorList.add(R.color.veryDarkBlue);
            colorList.add(R.color.veryDarkGreen);
            colorList.add(R.color.veryDarkPink);
            colorList.add(R.color.Maroon);
            colorList.add(R.color.veryDarkYellow);
        }

        return colorList;
    }

    public static void showToast(Activity activity, String message, int gravity) {
        try {
            Toast toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        } catch (Exception e) {
            Log.i(TAG_TOAST, "Toast failed. " + e);
        }
    }
}
