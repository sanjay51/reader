package Firebase;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Story;

import static Utils.Constants.CHILD_SEPARATOR;

/**
 * Created by sanjav on 5/22/16.
 */

public class CommentEntity extends FirebaseEntity implements CommentsObservable {
    Story story;
    String userEmail;
    String name;
    String comment;
    String photoURL;
    Long lastUpdated;

    List<CommentsObserver> observers = new ArrayList<>();

    public CommentEntity(Story story, String userEmail) {
        this.story = story;
        if (userEmail != null) this.userEmail = userEmail.replace(".", ",");
    }

    public CommentEntity withName(String name) {
        this.name = name;
        return this;
    }

    public CommentEntity withPhotoURL(String photoURL) {
        this.photoURL = photoURL;
        return this;
    }

    public CommentEntity withComment(String comment) {
        this.comment = comment;
        return this;
    }

    public CommentEntity withLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    @Override
    public String getReferenceStr() {
        return "StoryComments/" + story.getPrettyID() + CHILD_SEPARATOR + userEmail;
    }

    @Override
    public Map<String, Object> getChildUpdates() {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/comment", comment);
        childUpdates.put("/name", name);
        childUpdates.put("/status", Status.ACTIVE.toString());
        childUpdates.put("/photoURL", photoURL);

        return childUpdates;
    }

    @Override
    public String getReferenceStrForAll() {
        return "StoryComments/" + story.getPrettyID();
    }

    @Override
    public String getValue() {
        return comment;
    }

    public String getUserEmail() { return userEmail.replace(",", "."); }

    public String getDisplayName() { return name; }

    @Override
    public void populateFromDataSnapshot(DataSnapshot snapshot) {
        this.userEmail = snapshot.getKey();
        this.name = (String) snapshot.child("name").getValue();
        this.photoURL = (String) snapshot.child("photoURL").getValue();
        this.comment = (String) snapshot.child("comment").getValue();
        notifyObservers(CommentsObserver.CODE.COMMENT_LOADED, this);
    }

    @Override
    public void populateAll(DataSnapshot snapshot) {
        List<CommentEntity> comments = new ArrayList<>();
        for (DataSnapshot child : snapshot.getChildren()) {
            CommentEntity commentEntity = new CommentEntity(story, child.getKey())
                    .withName((String) child.child("name").getValue())
                    .withComment((String) child.child("comment").getValue())
                    .withPhotoURL((String) child.child("photoURL").getValue())
                    .withLastUpdated(((Long) child.child("lastUpdated").getValue()));

            comments.add(commentEntity);
        }

        Collections.sort(comments, new Comparator<CommentEntity>() {
            @Override
            public int compare(CommentEntity commentEntity, CommentEntity t1) {
                if (commentEntity.lastUpdated == t1.lastUpdated) return  0;
                return (commentEntity.lastUpdated > t1.lastUpdated ? -1 : 1);
            }
        });

        notifyObservers(CommentsObserver.CODE.ALL_COMMENTS_LOADED, comments);
    }

    @Override
    public void registerObserver(CommentsObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(CommentsObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(CommentsObserver.CODE code, Object update) {
        for (CommentsObserver observer : observers) {
            observer.notify(code, update);
        }
    }

    @Override
    public String toString() {
        return "userEmail: " + getUserEmail() + ", name: " + name + ", photo: " + photoURL + ", comment: " + comment;
    }
}
