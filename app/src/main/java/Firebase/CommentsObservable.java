package Firebase;

/**
 * Created by sanjav on 5/22/16.
 */

public interface CommentsObservable {
    public void registerObserver(CommentsObserver observer);
    public void removeObserver(CommentsObserver observer);
    public void notifyObservers(CommentsObserver.CODE code, Object update);
}
