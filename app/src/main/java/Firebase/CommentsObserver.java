package Firebase;

/**
 * Created by sanjav on 5/22/16.
 */

public interface CommentsObserver {
    public enum CODE { COMMENT_LOADED, ALL_COMMENTS_LOADED};
    public void notify(CODE code, Object commentEntity);
}
