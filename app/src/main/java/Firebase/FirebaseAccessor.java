package Firebase;

//import com.firebase.client.Firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

/**
 * Created by sanjav on 1/24/16.
 */
public class FirebaseAccessor {
    private static FirebaseAccessor firebaseAccessor;
    private static FirebaseDatabase firebaseDatabase;

    private FirebaseAccessor() {
        firebaseDatabase = FirebaseDatabase.getInstance();
    }

    public static FirebaseAccessor getInstance() {
        if (firebaseAccessor == null) firebaseAccessor = new FirebaseAccessor();

        return firebaseAccessor;
    }

    public static FirebaseDatabase getFirebaseDatabase() {
        if (firebaseDatabase == null) getInstance();

        return firebaseDatabase;
    }
}
