package Firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sanjav on 5/22/16.
 */

public abstract class FirebaseEntity {
    protected enum Status { ACTIVE, INACTIVE };

    public abstract String getReferenceStr();
    public abstract String getReferenceStrForAll();
    public abstract String getValue();
    public abstract Map<String, Object> getChildUpdates();
    public abstract void populateFromDataSnapshot(DataSnapshot snapshot);
    public abstract void populateAll(DataSnapshot snapshot);

    public void save() {
        DatabaseReference dbRef = FirebaseAccessor.getFirebaseDatabase().getReference(getReferenceStr().replace(".", ","));

        Map<String, Object> childUpdates = getChildUpdates();
        childUpdates.put("lastUpdated", ServerValue.TIMESTAMP);

        dbRef.updateChildren(childUpdates);
    }

    public void load() {
        DatabaseReference dbRef = FirebaseAccessor.getFirebaseDatabase().getReference(getReferenceStr());

        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                populateFromDataSnapshot(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //do something
            }
        });
    }

    public void loadAll() {
        DatabaseReference dbRef = FirebaseAccessor.getFirebaseDatabase().getReference(getReferenceStrForAll());
        dbRef.orderByChild("lastUpdated");

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                populateAll(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //do something
            }
        });
    }
}
