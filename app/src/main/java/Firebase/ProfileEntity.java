package Firebase;

import com.google.firebase.database.DataSnapshot;

import java.util.HashMap;
import java.util.Map;
import static Utils.Constants.*;

import Authentication.UserProfile;

public class ProfileEntity extends FirebaseEntity {
    UserProfile userProfile;
    private String PROFILE_PARENT_KEY = "Profile";

    public ProfileEntity(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public String getReferenceStr() {
        return PROFILE_PARENT_KEY + CHILD_SEPARATOR + userProfile.getEmail();
    }

    @Override
    public String getReferenceStrForAll() {
        return PROFILE_PARENT_KEY;
    }

    @Override
    public String getValue() {
        return null;
    }

    @Override
    public Map<String, Object> getChildUpdates() {
        Map<String, Object> childUpdates = userProfile.getAsMap();
        childUpdates.put(STATUS, ACTIVE_STATUS);

        return childUpdates;
    }

    @Override
    public void populateFromDataSnapshot(DataSnapshot snapshot) {

    }

    @Override
    public void populateAll(DataSnapshot snapshot) {

    }
}
