package Utils;

/**
 * Created by sanjav on 5/22/16.
 */

public class Constants {
    public static String DISC = "::";

    public static String EMAIL = "EMAIL";
    public static String DISPLAY_NAME = "DISPLAY_NAME";
    public static String IS_SIGNED_ID = "IS_SIGNED_IN";
    public static String PHOTO_URL = "PHOTO_URL";

    public static String STATUS = "STATUS";
    public static String ACTIVE_STATUS = "ACTIVE";
    public static String CHILD_SEPARATOR = "/";
}
