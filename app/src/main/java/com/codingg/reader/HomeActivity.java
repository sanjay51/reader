package com.codingg.reader;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.firebase.client.Firebase;

import java.util.ArrayList;
import java.util.List;

import Authentication.UserProfile;
import Data.StoryDataManager;
import Data.UserDataManager;
import Data.Utils;
import Authentication.SignInAccessor;
import model.MenuItem;
import model.Volume;


public class HomeActivity extends AppCompatActivity {
    private LinearLayout llVolumes;
    private LinearLayout llBooks;
    private LinearLayout llPrefMenu;
    private LinearLayout llOnlineContent;

    private HomeActivity This = this;

    SignInAccessor signInAccessor = SignInAccessor.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        UserDataManager.init(this.getApplicationContext());
        customize();
    }

    public void customize() {
        llVolumes = (LinearLayout) findViewById(R.id.llVolumes);
        llBooks = (LinearLayout) findViewById(R.id.llBooks);
        llPrefMenu = (LinearLayout) findViewById(R.id.llPrefMenu);
        //llOnlineContent = (LinearLayout) findViewById(R.id.llOnlineContent);

        initInspirationalVolumes();
        initBooks();
        //initOnlineContent();
        initOtherMenuItems();
        UserProfile.getInstance().initialize(this);
    }

    public void initInspirationalVolumes() {
        ArrayList<MenuItem> volumes = StoryDataManager.getVolumeList();
        llVolumes.removeAllViews();
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);

        int count = 0;
        for (MenuItem vol : volumes) {
            final Volume volume = (Volume) vol;
            List<Integer> colorList = Utils.getDarkColorList();

            if (volume.getVolumeType() == Volume.TYPE_MAIN) {
                View view = vi.inflate(R.layout.home_list_item, null);
                ((TextView) view.findViewById(R.id.tvVolumeNumber1)).setText(volume.getTitle());
                ((TextView) view.findViewById(R.id.tvVolumeNumber2)).setText(volume.getTitle());
                ((TextView) view.findViewById(R.id.tvAuthor)).setText(volume.getStoryCount() + "");
                ((TextView) view.findViewById(R.id.tvOneLiner)).setText(volume.getOneLiner());
                ((LinearLayout) view.findViewById(R.id.llDetails)).setBackgroundColor(getResources().getColor(colorList.get(count)));

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(This, VolumeActivity.class);
                        i.putExtra("volume", volume.getIndex() + "");
                        startActivity(i);
                    }
                });

                llVolumes.addView(view);
            }

            count++;
        }
    }

    public void initBooks() {
        ArrayList<MenuItem> volumes = StoryDataManager.getVolumeList();
        llBooks.removeAllViews();
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);

        for (MenuItem vol : volumes) {
            final Volume volume = (Volume) vol;

            if (volume.getVolumeType() == Volume.TYPE_BOOK) {
                View view = vi.inflate(R.layout.home_list_item_image, null);
                ((ImageView) view.findViewById(R.id.ivBookCover)).setImageResource(volume.getImageResourceID());
                ((TextView) view.findViewById(R.id.tvBookTitle)).setText(volume.getTitle());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(This, VolumeActivity.class);
                        i.putExtra("volume", volume.getIndex() + "");
                        startActivity(i);
                    }
                });

                llBooks.addView(view);
            }
        }
    }

    // Not currently used
    public void initOnlineContent() {
        ArrayList<MenuItem> items = StoryDataManager.getOnlineContentItems();
        llOnlineContent.removeAllViews();
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);

        for (MenuItem item : items) {
            if (item.getItemType() == MenuItem.ITEM_ONLINE_CONTENT) {
                View view = vi.inflate(R.layout.home_list_item_image, null);
                ((ImageView) view.findViewById(R.id.ivBookCover)).setImageResource(item.getImageResourceID());
                ((TextView) view.findViewById(R.id.tvBookTitle)).setText(item.getTitle());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(This, OnlineContentActivity.class);
                        startActivity(i);
                    }
                });

                llOnlineContent.addView(view);
            }
        }
    }

    public void initOtherMenuItems() {
        ArrayList<MenuItem> menuItems = StoryDataManager.getOtherMenuItems();
        llPrefMenu.removeAllViews();
        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);

        for (final MenuItem menuItem : menuItems) {
            View view = vi.inflate(R.layout.home_other_item, null);
            ((ImageView) view.findViewById(R.id.ivIcon)).setImageResource(menuItem.getImageResourceID());
            ((TextView) view.findViewById(R.id.tvTitle)).setText(menuItem.getTitle());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (menuItem.getItemType() == MenuItem.ITEM_YOUR_PROFILE) {
                        Intent i = new Intent(This, SignInActivity.class);
                        startActivity(i);
                    } else if (menuItem.getItemType() == MenuItem.ITEM_LIKE) {
                        likeApp();
                    } else if (menuItem.getItemType() == MenuItem.ITEM_SHARE) {
                        shareApp();
                    } else if (menuItem.getItemType() == MenuItem.ITEM_SUBMIT_STORY) {
                        Intent intent = new Intent(This, SubmitStoryActivity.class);
                        startActivity(intent);
                    } else if (menuItem.getItemType() == MenuItem.ITEM_CONTACT) {
                        contactDeveloper();
                    } else if (menuItem.getItemType() == MenuItem.ITEM_OTHER) {
                        Intent intent = new Intent(This, HomeActivity.class);
                        startActivity(intent);
                    } else if (menuItem.getItemType() == MenuItem.ITEM_BRAIN_SUITE) {
                        gotoMarketplace("com.codingg.readerBTFree");
                    }
                }
            });

            if (!(menuItem.getItemType() == MenuItem.ITEM_OTHER))
                if (menuItem.getItemType() == MenuItem.ITEM_BRAIN_SUITE) {
                    ((LinearLayout) view.findViewById(R.id.llBase))
                            .setBackgroundResource(R.drawable.highlighted_item_bg);
                }

                llPrefMenu.addView(view);
        }
    }

    public void likeApp() {
        gotoMarketplace("com.codingg.reader");
    }

    public void gotoMarketplace(String appID) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appID));
        startActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

        Uri uri = Uri.parse("market://details?id=" + appID);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appID)));
        }
    }

    public void shareApp() {
        String body = "hey, check this app \"Inspirational Short Stories\", " +
                "it's awesome.. here is the playstore link: https://play.google.com/store/apps/details?id=com.codingg.reader";
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, "Share this app"));
    }

    public void contactDeveloper() {
        Utils.emailDeveloper(This, "Contact", "");
    }
}