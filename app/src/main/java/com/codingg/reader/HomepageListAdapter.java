package com.codingg.reader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import model.MenuItem;
import model.Story;
import model.Volume;

/**
 * Created by sanjav on 12-Jul-14.
 */
public class HomepageListAdapter extends ArrayAdapter<String> {

    HashMap<Integer, MenuItem> mIdMap = new HashMap<Integer, MenuItem>();
    private static LayoutInflater inflater = null;
    private int itemLayout = R.layout.volume_list_item;

    private static Map<Integer, View> viewMap = new HashMap<>();

    public HomepageListAdapter(Context context, int textViewResourceId,
                               ArrayList<MenuItem> objects, int itemLayout) {
        super(context, textViewResourceId);

        if (itemLayout != 0) {
            this.itemLayout = itemLayout;
        }

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(i, objects.get(i));
            super.add(String.valueOf(i));
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;

        //if (viewMap.get(position) != null) {
          //  return viewMap.get(position);
        //}

        //if (vi == null) {
            vi = inflater.inflate(this.itemLayout, null);
            TextView tvHeading = (TextView) vi.findViewById(R.id.tvHeading);

            MenuItem item = mIdMap.get(position);
            tvHeading.setText(item.getTitle());

            TextView tvSmallText = (TextView) vi.findViewById(R.id.tvSmallText);
            tvSmallText.setText(getSmallText(item.getOneLiner()));

            if (item.getItemType() == MenuItem.ITEM_VOLUME) {
                TextView tvCount = (TextView) vi.findViewById(R.id.tvCount);
                tvCount.setText(((Volume)item) .getStoryCount() + "");
                tvCount.setVisibility(View.VISIBLE);

                TextView tvShortStories = (TextView) vi.findViewById(R.id.tvShortStories);
                tvShortStories.setVisibility(View.VISIBLE);

                ImageView ivBox = (ImageView) vi.findViewById(R.id.ivBox);
                ivBox.setVisibility(View.VISIBLE);
            }

            if (item.getItemType() == MenuItem.ITEM_LIKE) {
                ImageView ivLike = (ImageView) vi.findViewById(R.id.ivLike);
                ivLike.setVisibility(View.VISIBLE);
            }

            if (item.getItemType() == MenuItem.ITEM_SHARE) {
                ImageView ivShare = (ImageView) vi.findViewById(R.id.ivShare);
                ivShare.setVisibility(View.VISIBLE);
            }

            if (item.getItemType() == MenuItem.ITEM_SUBMIT_STORY) {
                ImageView ivSubmitStory = (ImageView) vi.findViewById(R.id.ivSubmitStory);
                ivSubmitStory.setVisibility(View.VISIBLE);
            }

            if (item.getItemType() == MenuItem.ITEM_CONTACT) {
                ImageView ivContact = (ImageView) vi.findViewById(R.id.ivContact);
                ivContact.setVisibility(View.VISIBLE);
            }
//        }

        //viewMap.put(position, vi);
        return vi;
    }

    private String getSmallText(String text) {
        StringBuilder smallText = new StringBuilder(34);
        smallText.append(text.substring(0, 80 < text.length() ? 80: text.length()));
        smallText.append("...");
        return smallText.toString();
    }

}