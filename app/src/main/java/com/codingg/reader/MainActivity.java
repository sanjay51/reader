package com.codingg.reader;

import java.util.ArrayList;

import Data.StoryDataManager;
import Data.UserDataManager;
import Data.Utils;
import model.MenuItem;
import model.Story;
import model.Volume;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {
    private LinearLayout llContentMain = null;

    public Activity This = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        This = this;

        llContentMain = (LinearLayout) findViewById(R.id.llContentMain);

        UserDataManager.init(this.getApplicationContext());
        initializeIndex();
        customize();
    }

    private void customize() {
        if (Utils.isAdFreeVersion()) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) llContentMain.getLayoutParams();
            params.bottomMargin = 10;
            llContentMain.setLayoutParams(params);
        }
    }

    private void initializeIndex() {
        final ListView listview = (ListView) findViewById(R.id.listIndex);

        final ArrayList<MenuItem> itemsList =  StoryDataManager.getVolumeList();
        final ArrayList<MenuItem> otherItemsList = StoryDataManager.getOtherMenuItems();
        itemsList.addAll(otherItemsList);

        final HomepageListAdapter adapter = new HomepageListAdapter(this,
                android.R.layout.simple_list_item_1, itemsList, R.layout.volume_list_item);
        listview.setAdapter(adapter);

        listview.setDivider(null);
        listview.setDividerHeight(0);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                if (itemsList.get(position).getItemType() == MenuItem.ITEM_LIKE) {
                    likeApp();
                } else if (itemsList.get(position).getItemType() == MenuItem.ITEM_SHARE) {
                    shareApp();
                } else if (itemsList.get(position).getItemType() == MenuItem.ITEM_SUBMIT_STORY) {
                    Intent intent = new Intent(This, SubmitStoryActivity.class);
                    startActivity(intent);
                } else if (itemsList.get(position).getItemType() == MenuItem.ITEM_CONTACT) {
                    contactDeveloper();
                } else if (itemsList.get(position).getItemType() == MenuItem.ITEM_OTHER) {
                    Intent intent = new Intent(This, HomeActivity.class);
                    startActivity(intent);
                }
                else {
                    Intent i = new Intent(This, VolumeActivity.class);
                    i.putExtra("volume", position + "");
                    startActivity(i);
                }
            }

        });
    }

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);
            return rootView;
        }
    }

    public void likeApp() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.codingg.reader"));
        startActivity(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);

        Uri uri = Uri.parse("market://details?id=com.codingg.reader");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.codingg.reader")));
        }
    }

    public void shareApp() {
        String body = "hey, check this app \"Inspirational Short Stories\", " +
                "it's awesome.. here is the playstore link: https://play.google.com/store/apps/details?id=com.codingg.reader";
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, "Share this app"));
    }

    public void contactDeveloper() {
        Utils.emailDeveloper(This, "Contact", "");
    }
}
