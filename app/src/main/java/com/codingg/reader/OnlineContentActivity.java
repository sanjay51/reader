package com.codingg.reader;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import Data.StaticData;
import Data.StoryDataManager;
import Data.Utils;
import model.Story;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

public class OnlineContentActivity extends ActionBarActivity {

    private WebView wvMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        initializeIndex();
        wvMain = (WebView) findViewById(R.id.wvMain);
        wvMain.getSettings().setJavaScriptEnabled(true);
        wvMain.setWebViewClient(new MyBrowser());
        wvMain.loadUrl("http://bit.ly/InspirationalShortStories");
    }

    private void initializeIndex() {
        //initialize view variables
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeIndex();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
