package com.codingg.reader;

import Authentication.AuthenticationObserver;
import Authentication.SignInAccessor;
import Authentication.UserProfile;
import Data.StoryDataManager;
import Data.UserDataManager;
import Data.Utils;
import Firebase.CommentEntity;
import Firebase.CommentsObserver;
import model.Story;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ReadActivity extends AppCompatActivity implements CommentsObserver, AuthenticationObserver {
    private LinearLayout llContentRead;
    private LinearLayout llPrefPane;
    private LinearLayout llFiller;
    private LinearLayout llComments;

    LayoutInflater layoutInflater;

    private ScrollView svContent;

    private RelativeLayout rlRoot;

    private Button btnPlayTeaser;
    private Button btnShare;
    private Button btnShare2;
    private Button btnPreferences;
    private Button btnNextStory;

    private TextView tvStory;
    private TextView tvTitle;

    // Font view controls
    private Button btnTextSizePlus;
    private Button btnTextSizeMinus;
    private Spinner spChangeFont;
    private Spinner spChangeColor;

    // Animations
    private Animation animFadeIn;
    private Animation animFadeOut;
    private Animation leftMoveIn;
    private Animation leftMoveOut;

    private Story story;
    private TextToSpeech textToSpeech;
    private int storyIndex = 0;
    private int volume = 0;

    // Comments
    CommentEntity commentEntity;
    private Button btnAddComment;
    private EditText etComment;
    private LinearLayout llSignInNotice;
    private LinearLayout llAddComment;

    AppCompatActivity This = this;

    List<CommentEntity> comments;
    private String currentCommentValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_read);

        llContentRead = (LinearLayout) findViewById(R.id.llContentRead);
        rlRoot = (RelativeLayout) findViewById(R.id.rlRoot);

        llFiller = (LinearLayout) findViewById(R.id.llFiller);
        llFiller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrefPane(false);
            }
        });

        tvStory = (TextView)findViewById(R.id.tvStory);
        storyIndex = Integer.parseInt(getIntent().getExtras().getString("position"));
        volume = Integer.parseInt(getIntent().getExtras().getString("volume"));

        svContent = (ScrollView) findViewById(R.id.svContent);

        UserDataManager.init(this.getApplicationContext());
        loadStoryComponents();

        SignInAccessor.getInstance().registerObserver(this);
    }

    private void finalizeView() {
        showPrefPane(false);
    }

    private void loadStoryComponents() {
        StoryDataManager dataManager = StoryDataManager.getManager(this.getApplicationContext(), volume);

        if (dataManager.isLastStory(storyIndex)) {
            Utils.showToast(This, "You have reached the end of this volume. Please go back and select another.",
                    Gravity.CENTER);
            return;
        }

        story = dataManager.getStory(storyIndex);
        tvStory.setText(story.getContent());

        tvTitle = (TextView)findViewById(R.id.tvTitle);
        tvTitle.setText(story.getIndexPlusOne() + ". " + story.getTitle());

        initializeIndex();
        updateRatingOnUI();
        initializeAnimation();
        customize();
        finalizeView();
        markAsRead();
        svContent.scrollTo(0, 0);

        commentEntity = new CommentEntity(story, null);
        commentEntity.registerObserver(this);
        commentEntity.loadAll();

        initCommentView();
    }

    private void initCommentView() {
        llSignInNotice = (LinearLayout) findViewById(R.id.llSignInNotice);
        llAddComment  = (LinearLayout) findViewById(R.id.llAddComment);

        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);

        //sign in button
        this.findViewById(R.id.read_activity_sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(This, SignInActivity.class);
                startActivity(i);
            }
        });

        reactOnSignInStatus();
    }

    private void reactOnSignInStatus() {
        if (SignInAccessor.isSignedIn()) {
            llSignInNotice.setVisibility(View.GONE);
            llAddComment.setVisibility(View.VISIBLE);
            updateNewCommentText();
        } else {
            llSignInNotice.setVisibility(View.VISIBLE);
            llAddComment.setVisibility(View.GONE);
        }

    }

    private void initializeIndex() {
        textToSpeech=new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if(status != TextToSpeech.ERROR){
                            textToSpeech.setLanguage(Locale.US);
                        }
                    }
                });

        btnPreferences = (Button) findViewById(R.id.btnPreferences);
        btnPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrefPane(true);
            }
        });

        btnPlayTeaser = (Button) findViewById(R.id.btnPlayTeaser);
        btnPlayTeaser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSpeakText();
            }
        });

        btnShare = (Button) findViewById (R.id.btnShare);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareStory();
            }
        });

        btnShare2 = (Button) findViewById (R.id.btnShare2);
        btnShare2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareStory();
            }
        });

        btnTextSizePlus = (Button) findViewById(R.id.btnTextSizePlus);
        btnTextSizePlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTextSize(1);
            }
        });

        btnTextSizeMinus = (Button) findViewById(R.id.btnTextSizeMinus);
        btnTextSizeMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTextSize(-1);
            }
        });

        btnNextStory = (Button) findViewById(R.id.btnNextStory);
        btnNextStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNextStory();
            }
        });

        spChangeFont = (Spinner) findViewById(R.id.spChangeFont);
        spChangeFont.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeFont(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //do nothing
            }
        });

        spChangeColor = (Spinner) findViewById(R.id.spChangeColor);
        spChangeColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeTheme(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etComment = (EditText) findViewById(R.id.etComment);

        btnAddComment = (Button) findViewById(R.id.btnAddComment);
        btnAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addComment();
            }
        });

        layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(this.getApplicationContext().LAYOUT_INFLATER_SERVICE);


        llPrefPane = (LinearLayout) findViewById (R.id.llPrefPane);
        llComments = (LinearLayout) findViewById(R.id.llComments);
    }

    private void addComment() {
        String commentText = etComment.getText().toString();
        if (commentText.equals("add note here..")
                || commentText.equals(currentCommentValue)) {
            Utils.showToast(this, "Please add/update note", Gravity.BOTTOM);
            return;
        }

        new CommentEntity(story, SignInAccessor.getUserData(SignInAccessor.USER_DATA_CODE.EMAIL))
                .withName(UserProfile.getInstance().getDisplayName())
                .withComment(etComment.getText().toString())
                .withPhotoURL(UserProfile.getInstance().getPhotoURL())
                .save();
    }

    private void initializeAnimation() {
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        leftMoveIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_move_in);
        leftMoveOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.left_move_out);
    }

    private void markAsRead() {
        UserDataManager manager = UserDataManager.getInstance(this.getApplicationContext());
        manager.markStoryRead(getVolume(), story.getIndex());
    }

    private String getVolume() {
        return "volume" + volume;
    }

    @Override
    public void onBackPressed(){
        if(textToSpeech !=null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onBackPressed();
    }

    private void customize() {
        if (Utils.isAdFreeVersion()) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) llContentRead.getLayoutParams();
            params.bottomMargin = 10;
            llContentRead.setLayoutParams(params);
        }
    }

    boolean isSpeaking = false;
    public void toggleSpeakText(){
        if (isSpeaking) {
            textToSpeech.stop();
            isSpeaking = false;
            return;
        }

        String toSpeak = story.getContent();
        Toast.makeText(getApplicationContext(), toSpeak,
                Toast.LENGTH_SHORT).show();
        textToSpeech.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
        isSpeaking = true;
    }

    public void shareStory() {
        String footer = "\n\n - Sent from Inspirational Short Stories (android app), https://play.google.com/store/apps/details?id=com.codingg.reader";
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, story.getTitle());
        sharingIntent.putExtra(Intent.EXTRA_TEXT, story.getTitle() + "\n\n" + story.getContent() + footer);
        startActivity(Intent.createChooser(sharingIntent, "Share story using"));
    }

    public void changeTextSize(float byValue) {
        tvStory.setTextSize(TypedValue.COMPLEX_UNIT_PX, tvStory.getTextSize() + byValue);
    }

    public void changeFont(int position) {
        switch (position) {
            case 0:
                tvStory.setTypeface(Typeface.DEFAULT);
                break;
            case 1:
                tvStory.setTypeface(Typeface.MONOSPACE);
                break;
            case 2:
                tvStory.setTypeface(Typeface.SERIF);
                break;

            default:
                tvStory.setTypeface(Typeface.DEFAULT);
        }
    }

    private void changeTheme(int position) {
        switch (position) {
            case 0:
                setTheme(ReadTheme.BLACK_OVER_WHITE);
                break;
            case 1:
                setTheme(ReadTheme.WHITE_OVER_BLACK);
                break;
            case 2:
                setTheme(ReadTheme.GREEN_OVER_LIGHT_GREEN);
                break;
            case 3:
                setTheme(ReadTheme.SEPIA);
                break;

            default:
                setTheme(ReadTheme.BLACK_OVER_WHITE);
        }
    }

    private void showPrefPane(boolean showPane) {

        if (showPane) {
            llPrefPane.setVisibility(View.VISIBLE);
            llFiller.setVisibility(View.VISIBLE);
            llPrefPane.startAnimation(leftMoveIn);
        } else {
            llPrefPane.setVisibility(View.GONE);
            llFiller.setVisibility(View.GONE);
            llPrefPane.startAnimation(leftMoveOut);
        }
    }

    private void setTheme(ReadTheme theme) {
        rlRoot.setBackgroundColor(theme.backgroundColor);
        tvStory.setTextColor(theme.textColor);
        tvTitle.setTextColor(theme.titleColor);
    }

    static class ReadTheme {
        public static final ReadTheme WHITE_OVER_BLACK = new ReadTheme(Color.BLACK, Color.WHITE, Color.WHITE);
        public static final ReadTheme BLACK_OVER_WHITE = new ReadTheme(Color.WHITE, Color.BLACK, Color.BLACK);
        public static final ReadTheme GREEN_OVER_LIGHT_GREEN = new ReadTheme(Color.parseColor("#bbd0a7"), Color.parseColor("#3d6d0e"),  Color.parseColor("#3d6d0e"));
        public static final ReadTheme SEPIA = new ReadTheme(Color.parseColor("#f0e8d1"), Color.BLACK, Color.BLACK);

        public int backgroundColor;
        public int textColor;
        public int titleColor;

        private ReadTheme(int backgroundColor, int textColor, int titleColor) {
            this.backgroundColor = backgroundColor;
            this.textColor = textColor;
            this.titleColor = titleColor;
        }
    }

    private void loadNextStory() {
        this.textToSpeech.stop();
        storyIndex++;
        loadStoryComponents();
    }

    static final Map<Integer, Integer> viewToRatingMap = new HashMap<>();
    static final Map<Integer, Integer> ratingToViewMap = new HashMap<>();
    static {
        viewToRatingMap.put(R.id.rate1star, 1);
        ratingToViewMap.put(1, R.id.rate1star);

        viewToRatingMap.put(R.id.rate2star, 2);
        ratingToViewMap.put(2, R.id.rate2star);

        viewToRatingMap.put(R.id.rate3star, 3);
        ratingToViewMap.put(3, R.id.rate3star);

        viewToRatingMap.put(R.id.rate4star, 4);
        ratingToViewMap.put(4, R.id.rate4star);

        viewToRatingMap.put(R.id.rate5star, 5);
        ratingToViewMap.put(5, R.id.rate5star);
    }

    public void onToggleStar(View view) {
        rateStory(viewToRatingMap.get(view.getId()));
    }

    private void rateStory(int stars) {
        // store
        UserDataManager manager = UserDataManager.getInstance(this.getApplicationContext());
        manager.setStoryRating(getVolume(), story.getId(), stars);

        updateRatingOnUI();
    }

    private void updateRatingOnUI() {
        UserDataManager manager = UserDataManager.getInstance(this.getApplicationContext());
        int stars = manager.getStoryRatingLocal(getVolume(), story.getId());

        for (int $i = 1; $i <= stars; $i++) {
            ((ImageView) findViewById(ratingToViewMap.get($i)))
                    .setImageResource(R.drawable.star_pressed);
        }

        for (int $i = stars+1; $i <= 5; $i++) {
            ((ImageView) findViewById(ratingToViewMap.get($i)))
                    .setImageResource(R.drawable.star);
        }
    }

    @Override
    public void notify(CODE code, Object commentsEntity) {
        if (code == CODE.ALL_COMMENTS_LOADED) {
            comments = (List<CommentEntity>) commentsEntity;

            llComments.removeAllViews();
            for (CommentEntity comment : comments) {
                View view = layoutInflater.inflate(R.layout.comment_item, null);
                ((TextView) view.findViewById(R.id.tvCommentName)).setText(comment.getDisplayName());
                ((TextView) view.findViewById(R.id.tvCommentText)).setText(comment.getValue());

                llComments.addView(view);
            }

            updateNewCommentText();
        }
    }

    private void updateNewCommentText() {

        if (comments != null) {
            for (CommentEntity comment : comments) {
                if (comment.getUserEmail().equals(SignInAccessor.getInstance().getUserData(SignInAccessor.USER_DATA_CODE.EMAIL))) {
                    currentCommentValue = comment.getValue();
                    ((EditText) findViewById(R.id.etComment)).setText(comment.getValue());
                    break;
                }
            }
        }
    }

    @Override
    public void notify(AUTH_CODE authCode, Object update) {
        if (authCode == AUTH_CODE.SIGNED_IN) reactOnSignInStatus();
    }

    @Override
    public void onDestroy() {
        SignInAccessor.getInstance().removeObserver(this);
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        //reactOnSignInStatus();
    }
}
