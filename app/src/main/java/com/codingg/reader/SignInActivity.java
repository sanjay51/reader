package com.codingg.reader;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import Authentication.AuthenticationObserver;
import Authentication.SignInAccessor;
import Data.Utils;

import static android.view.Gravity.BOTTOM;

public class SignInActivity extends AppCompatActivity implements AuthenticationObserver, GoogleApiClient.OnConnectionFailedListener {
    private SignInActivity This = this;
    public String TAG = "SIGN_IN_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        loadUIComponents();
        SignInAccessor.getInstance().registerObserver(this);
    }

    private void loadUIComponents() {
        this.findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignInAccessor.getInstance().signIn(This);
            }
        });

        this.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignInAccessor.getInstance().signOut(This);
            }
        });

        updateLocalUIOnSignIn();
    }

    public void updateLocalUIOnSignIn() {

        if (SignInAccessor.getInstance().isSignedIn()) {
            findViewById(R.id.llSignIn).setVisibility(View.GONE);
            findViewById(R.id.llAlreadySignedIn).setVisibility(View.VISIBLE);

            String displayName = SignInAccessor.getUserData(SignInAccessor.USER_DATA_CODE.DISPLAY_NAME);
                ((TextView) findViewById(R.id.tvWelcome)).setText("Hello there, " + displayName + "!");
        } else {
            findViewById(R.id.llSignIn).setVisibility(View.VISIBLE);
            findViewById(R.id.llAlreadySignedIn).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SignInAccessor.RC_SIGN_IN) {
            // Make sure the request was successful
            Log.i("SignInActivity", "Signed in code:: " + resultCode);
            SignInAccessor.getInstance().handleSignInResult(data);
        }
    }

    @Override
    public void notify(AUTH_CODE authCode, Object update) {
        updateLocalUIOnSignIn();

        switch (authCode) {
            case SIGNED_IN:
                Utils.showToast(this, "Signed in as " +
                        SignInAccessor.getUserData(SignInAccessor.USER_DATA_CODE.DISPLAY_NAME),
                        BOTTOM);
                this.finish();
                break;

            case LOGGED_OUT:
                Utils.showToast(this, "You have been logged out.", BOTTOM);
                break;

            case AUTH_FAILED:
                Utils.showToast(this, "Sign in failed, network issue.", BOTTOM);
                break;

            default:
                Utils.showToast(this, "Sign in failed.", BOTTOM);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed.");
    }
}
