package com.codingg.reader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import Data.UserDataManager;
import model.Story;

/**
 * Created by sanjav on 12-Jul-14.
 */
public class StableArrayAdapter extends ArrayAdapter<String> {

    final List<Story> originalStoryList;
    List<Story> storyList = new ArrayList<>();
    private static LayoutInflater inflater = null;
    private int itemLayout = R.layout.story_list_item;
    private Context context;

    public StableArrayAdapter(Context context, int textViewResourceId,
                              ArrayList<Story> objects, int itemLayout) {
        super(context, textViewResourceId);
        this.context = context;

        if (itemLayout != 0) {
            this.itemLayout = itemLayout;
        }

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < objects.size(); ++i) {
            storyList.add(objects.get(i));
            super.add(String.valueOf(i));
        }

        originalStoryList = new ArrayList<>();
        originalStoryList.addAll(storyList);
    }

    @Override
    public long getItemId(int position) {
        return storyList.get(position).getIndex();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null)
            vi = inflater.inflate(this.itemLayout, null);
        TextView tvHeading = (TextView) vi.findViewById(R.id.tvHeading);

        Story story = storyList.get(position);
        tvHeading.setText(story.getIndexPlusOne() + ". " + story.getTitle());

        TextView tvSmallText = (TextView) vi.findViewById(R.id.tvSmallText);
        tvSmallText.setText(getSmallText(story.getContent()));

        LinearLayout llReadStatus = (LinearLayout) vi.findViewById(R.id.llReadStatus);
        if (story.isRead())
            llReadStatus.setVisibility(View.VISIBLE);
        else
            llReadStatus.setVisibility(View.GONE);

        setLocalRating(vi, story);

        return vi;
    }

    private void setLocalRating(View view, Story story) {
        int localRating = UserDataManager.getInstance(context).getStoryRatingLocal(story.getVolume(), story.getId());

        if (localRating == 0) {
            view.findViewById(R.id.rating1star).setVisibility(View.GONE);
            view.findViewById(R.id.rating2star).setVisibility(View.GONE);
            view.findViewById(R.id.rating3star).setVisibility(View.GONE);
            view.findViewById(R.id.rating4star).setVisibility(View.GONE);
            view.findViewById(R.id.rating5star).setVisibility(View.GONE);

            view.findViewById(R.id.tvYouRated).setVisibility(View.GONE);
        } else {
            if (localRating >= 1) (view.findViewById(R.id.rating1star)).setVisibility(View.VISIBLE);
            if (localRating >= 2) (view.findViewById(R.id.rating2star)).setVisibility(View.VISIBLE);
            if (localRating >= 3) (view.findViewById(R.id.rating3star)).setVisibility(View.VISIBLE);
            if (localRating >= 4) (view.findViewById(R.id.rating4star)).setVisibility(View.VISIBLE);
            if (localRating >= 5) (view.findViewById(R.id.rating5star)).setVisibility(View.VISIBLE);

            view.findViewById(R.id.tvYouRated).setVisibility(View.VISIBLE);
        }
    }

    private String getSmallText(String text) {
        StringBuilder smallText = new StringBuilder(34);
        smallText.append(text.substring(0, 80 < text.length() ? 80: text.length()));
        smallText.append("...");
        return smallText.toString();
    }

    public void sortByReadStatus() {
        sortByNaturalOrder();
        List<Story> newList = new ArrayList<>();

        for (Story story : storyList) {
            if (story.isRead()) {
                newList.add(story);
            }
        }

        for (Story story : storyList) {
            if (! story.isRead()) {
                newList.add(story);
            }
        }

        storyList = newList;
    }

    public void sortByUnreadFirst() {
        sortByNaturalOrder();
        List<Story> newList = new ArrayList<>();

        for (Story story : storyList) {
            if (! story.isRead()) {
                newList.add(story);
            }
        }

        for (Story story : storyList) {
            if (story.isRead()) {
                newList.add(story);
            }
        }

        storyList = newList;
    }

    public void sortByYourRating() {
        sortByNaturalOrder();

        List<Story> list0star = new ArrayList<>();
        List<Story> list1star = new ArrayList<>();
        List<Story> list2star = new ArrayList<>();
        List<Story> list3star = new ArrayList<>();
        List<Story> list4star = new ArrayList<>();
        List<Story> list5star = new ArrayList<>();

        storyList = new ArrayList<>();

        final UserDataManager manager = UserDataManager.getInstance(context);
        for (Story story : originalStoryList) {
            int rating = manager.getStoryRatingLocal(story.getVolume(), story.getId());

            switch (rating) {
                case 1: list1star.add(story); break;
                case 2: list2star.add(story); break;
                case 3: list3star.add(story); break;
                case 4: list4star.add(story); break;
                case 5: list5star.add(story); break;
                default: list0star.add(story); break;
            }
        }

        storyList.addAll(list5star);
        storyList.addAll(list4star);
        storyList.addAll(list3star);
        storyList.addAll(list2star);
        storyList.addAll(list1star);
        storyList.addAll(list0star);
    }

    public void sortByNaturalOrder() {
        storyList = new ArrayList<>();
        storyList.addAll(originalStoryList);
    }

}