package com.codingg.reader;

import Data.StoryDataManager;
import Data.UserDataManager;
import Data.Utils;
import model.Story;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class SubmitStoryActivity extends ActionBarActivity {

    private Button btnEmailStory;
    SubmitStoryActivity This;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        //this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_submit_story);
        This = this;

        initializeIndex();
    }

    private void initializeIndex() {
        bindVariables();
    }

    private void bindVariables() {
        btnEmailStory = (Button) findViewById(R.id.btnEmailStory);

        btnEmailStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.emailDeveloper(This, "Submit story", "please write the story here..");
            }
        });
    }
}
