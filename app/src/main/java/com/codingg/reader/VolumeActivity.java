package com.codingg.reader;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import Data.StaticData;
import Data.StoryDataManager;
import Data.Utils;
import model.Story;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

public class VolumeActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener {
    private int Volume = 1;
    private LinearLayout llContentVolume = null;

    private StableArrayAdapter storyListAdapter;
    public Activity This = null;
    private int sortOrder = StaticData.SORT_NATURAL_ORDER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);
        Volume = Integer.parseInt(getIntent().getExtras().getString("volume"));
        This = this;

        llContentVolume = (LinearLayout) findViewById(R.id.llContentVolume);

        initializeIndex();
        customize();
        initializeSpinner();
        //initializeAd();
    }

    private void customize() {
        if (Utils.isAdFreeVersion()) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) llContentVolume.getLayoutParams();
            params.bottomMargin = 10;
            llContentVolume.setLayoutParams(params);
        }
    }

    private void initializeIndex() {
        final ListView listview = (ListView) findViewById(R.id.listIndex);

        final ArrayList<Story> list =  StoryDataManager.getManager(this.getApplicationContext(), Volume).getStoryList();

        storyListAdapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list, R.layout.story_list_item);
        listview.setAdapter(storyListAdapter);

        listview.setAlpha(0.9F);
        listview.setDivider(null);
        listview.setDividerHeight(0);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                Intent i = new Intent(This, ReadActivity.class);
                long storyIndex = storyListAdapter.getItemId(position);
                i.putExtra("position", storyIndex + "");
                i.putExtra("volume", Volume + "");
                //initializeAd();
                startActivity(i);
            }

        });
    }

    private void initializeSpinner() {
        Spinner spSortVolume = (Spinner) findViewById(R.id.spSortVolume);
        List<String> categories = new ArrayList<>();
        categories.add("Natural order");
        categories.add("Unread first");
        categories.add("Recently read first");
        categories.add("Rated by you first");

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, categories);
        spSortVolume.setOnItemSelectedListener(this);
        spSortVolume.setAdapter(spinnerAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeIndex();
        this.sort();
        //displayInterstitial();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //On spinner item selected - for sorting list
        if (position == 0) {
            sortOrder = StaticData.SORT_NATURAL_ORDER;
        } else if (position == 1) {
            sortOrder = StaticData.SORT_UNREAD_FIRST;
        } else if (position == 3) {
            sortOrder = StaticData.SORT_BY_YOUR_RATING;
        } else {
            sortOrder = StaticData.SORT_RECENTLY_READ_FIRST;
        }

        this.sort();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void sort() {
        switch (sortOrder) {
            case StaticData.SORT_NATURAL_ORDER:
                storyListAdapter.sortByNaturalOrder();
                break;
            case StaticData.SORT_RECENTLY_READ_FIRST:
                storyListAdapter.sortByReadStatus();
                break;
            case StaticData.SORT_UNREAD_FIRST:
                storyListAdapter.sortByUnreadFirst();
                break;

            case StaticData.SORT_BY_YOUR_RATING:
                storyListAdapter.sortByYourRating();
                break;

            default:
                storyListAdapter.sortByNaturalOrder();
                break;
        }

        storyListAdapter.notifyDataSetChanged();
    }
}
