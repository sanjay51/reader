package model;

/**
 * Created by sanjav on 9/5/15.
 */
public class MenuItem {
    public static final int ITEM_VOLUME = 1;
    public static final int ITEM_OTHER = 2;
    public static final int ITEM_LIKE = 3;
    public static final int ITEM_SHARE = 4;
    public static final int ITEM_SUBMIT_STORY = 5;
    public static final int ITEM_CONTACT = 6;
    public static final int ITEM_BRAIN_SUITE = 7;
    public static final int ITEM_ONLINE_CONTENT = 8;
    public static final int ITEM_YOUR_PROFILE = 9;

    private int index = 0;
    private String title = "";
    private String oneLiner = "";
    private int itemType = ITEM_OTHER;
    private int imageResourceID = 0;

    public MenuItem() {

    }

    public MenuItem(int index, String title, String oneLiner) {
        this.setIndex(index);
        this.setTitle(title);
        this.setOneLiner(oneLiner);
        this.setItemType(ITEM_OTHER);
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setOneLiner(String oneLiner) {
        this.oneLiner = oneLiner;
    }

    public String getOneLiner() {
        return oneLiner;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getItemType() {
        return itemType;
    }

    public int getImageResourceID() {
        return imageResourceID;
    }

    public void setImageResourceID(int imageResourceID) {
        this.imageResourceID = imageResourceID;
    }
}
