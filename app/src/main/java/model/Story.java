package model;

import static Utils.Constants.DISC;

public class Story {
    public int index;
    public String title;
    public String content;
    public String id;
    public String volume;
    public int tag = 0;
    private boolean isRead = false;

    public Story() {
        this.index = 0;
        this.tag = 0;
        this.volume = "volume0";
        this.title = "null";
        this.content = "null";
    }
    public Story(int index, String title, String content, int tag, String id, String volume) {
        this.title = title;
        this.content = content;
        this.index = index;
        this.tag = tag;
        this.id = id;
        this.volume = volume;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getTitle() {
        return this.title;
    }

    public int getTag() {
        return tag;
    }

    public int getIndex() {
        return this.index;
    }

    public String getId() {
        return this.id;
    }

    public int getIndexPlusOne() {
        //story indexes start from Zero, for presentation we need index+1
        return this.index + 1;
    }

    public String getVolume() {
        return String.valueOf(this.volume);
    }

    public String getContent() {
        return this.content;
    }

    public boolean isRead() {
        return isRead;
    }

    public String getPrettyID() {
        return "Story" + DISC + id;
    }
}
