package model;

import com.codingg.reader.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjav on 9/5/15.
 */
public class Volume extends MenuItem {
    private static List<Volume> allVolumes = new ArrayList<>();
    private int storyCount = 0;
    private int volumeType = 1;

    public static final int TYPE_MAIN = 1;
    public static final int TYPE_BOOK = 2;

    public Volume(int index, int storyCount, String title, String oneLiner) {
        this.storyCount = storyCount;
        this.setIndex(index);
        this.setOneLiner(oneLiner);
        this.setTitle(title);
        this.setItemType(MenuItem.ITEM_VOLUME);

        allVolumes.add(this);
    }

    public int getStoryCount() {
        return storyCount;
    }

    //static method
    public static List<Volume> getAllVolumes() {
        return allVolumes;
    }

    public void setVolumeType(int volumeType) {
        this.volumeType = volumeType;
    }

    public int getVolumeType() {
        return this.volumeType;
    }
}
